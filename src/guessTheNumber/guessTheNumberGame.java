package guessTheNumber;

import java.util.Random;

public class guessTheNumberGame {
	private int guessedNumber = 0;
	private int[] set1 = new int[7];
	private int[] set2 = new int[7];
	private int[] set3 = new int[7];
	private int[] deck = new int[21];

	public int getAnswer(){
		return this.guessedNumber;
	}

	public void setAnswer(int number){
		this.guessedNumber = this.deck[number] ;
		System.out.print("");
	}

	public void generateNumbers (int min,int max) {
		Random random = new Random();
		boolean exists = false;
		int randNumber, i = 0;

		while( i < 21) {
			exists = false;
			randNumber = random.nextInt((max - min) + 1) + min;

			for ( int j = 0; j < 21; j++){
					if ( this.deck[j] == randNumber){
						exists = true;
					}
				}
			if (exists == true){
				randNumber = random.nextInt((max - min) + 1) + min;
			} else if ( exists == false){
				this.deck[i] = randNumber;
				i++;
			}
		}
		/* for ( i = 0; i <21 ; i++){
			System.out.print(this.deck[i] + " ");
		}
		System.out.println(""); */

		i = 0;
		for (int j = 20 ; j > 0 ; j-=3) {
			this.set1[i] = this.deck[j];
			this.set2[i] = this.deck[j-1];
			this.set3[i] = this.deck[j-2];
			i++;
	}
		System.out.print("");
	}			
	
	public void shuffleDeck(int position) {
		
		int i = 0;
		switch(position){
			case 1:

				i = 0;
				for (int j = 0; j < 7; j++){
					this.deck[i] = this.set2[j]; //first group in this.deck
					this.deck[i+7] = this.set1[j]; //middle group in this.deck
					this.deck[i+14] = this.set3[j]; //third group in deck
					i++;
				}
			
				i = 0;
				for (int j = 20 ; j > 0 ; j-=3) {
					this.set1[i] = this.deck[j];
					this.set2[i] = this.deck[j-1];
					this.set3[i] = this.deck[j-2];
					i++;
				}

				break;

			case 2:								
				i = 0;
				for (int j = 0; j < 7; j++){
					this.deck[i] = this.set1[j]; //first group in this.deck
					this.deck[i+7] = this.set2[j]; //second group in this.deck
					this.deck[i+14] = this.set3[j]; //third group in this.deck
					i++;
				}
				i = 0;
				for (int j = 20 ; j > 0 ; j-=3) {
					this.set1[i] = this.deck[j];
					this.set2[i] = this.deck[j-1];
					this.set3[i] = this.deck[j-2];
					i++;
				}

				break;

			case 3:			
				i = 0;
				for (int j = 0; j < 7; j++){
					this.deck[i] = this.set1[j]; //first group in this.deck
					this.deck[i+7] = this.set3[j]; //second group in this.deck
					this.deck[i+14] = this.set2[j]; //third group in this.deck
					i++;
				}

				i = 0;
				for (int j = 20 ; j > 0 ; j-=3) {
					this.set1[i] = this.deck[j];
					this.set2[i] = this.deck[j-1];
					this.set3[i] = this.deck[j-2];
					i++;
				}
				
				break;
			default: 
			}
	}

	public void printSets(){

		int set=1;
		while (set <= 3){
			switch (set){
				case 1:
					System.out.print("\nSet 1: [");
					for (int i =0 ; i < 7; i++){

						System.out.print(this.set1[i]);
						if (i == 6){
							System.out.print("]");
						}
						else{
							System.out.print(", ");
						}
					}
					System.out.println();
					break;
				case 2:
					System.out.print("Set 2: [");
					for (int i =0 ; i < 7; i++){

						System.out.print(this.set2[i]);
						if (i == 6){
							System.out.print("]");
						}
						else{
							System.out.print(", ");
						}
					}
					System.out.println();
				break;
				case 3:
					System.out.print("Set 3: [");
					for (int i =0 ; i < 7; i++){

						System.out.print(this.set3[i]);
						if (i == 6){
							System.out.print("]");
						}
						else{
							System.out.print(", ");
						}
					}
					System.out.println();
					break;
				default:
			}
			set++;
		}
	}
}