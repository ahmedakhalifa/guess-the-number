package guessTheNumber;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		//int deck[] = {};
		guessTheNumberGame newGame = new guessTheNumberGame();
		System.out.println("\n\n\nWelcome to Guess the number application");
		System.out.println("Try to memorize a number from the 3 sets below.");
		System.out.println("Then choose which set does have the number in.\n\n\n");
		newGame.generateNumbers(1, 21);
		for ( int i = 0 ; i < 3; i++){
		 	newGame.printSets();
		 	System.out.print("\nIn which set the number is located in? ");
		 	Scanner sc = new Scanner(System.in);
	 		int pos = sc.nextInt();
	 		if ( pos <= 3 && pos >=1 ){
		 		newGame.shuffleDeck(pos);
	 		}
	 	}		
	 newGame.setAnswer(10);
	 System.out.println("\nYour number is: " + newGame.getAnswer());
	}

}
